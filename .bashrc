#!/usr/bin/env bash

#Write Branch name for Git
bold='';
reset="\e[0m";
black="\e[1;30m";
blue="\e[1;34m";
cyan="\e[1;36m";
green="\e[1;32m";
orange="\e[1;33m";
purple="\e[1;35m";
red="\e[1;31m";
violet="\e[1;35m";
white="\e[1;37m";
yellow="\e[1;33m";


PS1="\[${purple}\]\u@local\[${reset}\]";
PS1+=" in \[${yellow}\]\w\[${reset}\]";
PS1+=" on \[${green}\]\$(git branch 2>/dev/null | grep '^*' | colrm 1 2) \[${reset}\]";
PS1+="\$ ";

export PS1;
